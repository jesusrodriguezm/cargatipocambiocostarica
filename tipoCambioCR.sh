#!/bin/bash

#EJEMPLOS URLS
#urlCompra="http://indicadoreseconomicos.bccr.fi.cr/indicadoreseconomicos/WebServices/wsIndicadoresEconomicos.asmx/ObtenerIndicadoresEconomicosXML?tcIndicador=317&tcFechaInicio=19/11/2015&tcFechaFinal=19/11/2015&tcNombre=dmm&tnSubNiveles=N"
#urlVenta="http://indicadoreseconomicos.bccr.fi.cr/indicadoreseconomicos/WebServices/wsIndicadoresEconomicos.asmx/ObtenerIndicadoresEconomicosXML?tcIndicador=318&tcFechaInicio=19/11/2015&tcFechaFinal=19/11/2015&tcNombre=dmm&tnSubNiveles=N"

#OBTENER LA FECHA ACTUAL EN EL FORMATO DE LA LLAMADA AL WEB SERVICE
DATE=`date +%d/%m/%Y`
#echo $DATE

#COMPONER URL COMPRA
urlCompra="http://indicadoreseconomicos.bccr.fi.cr/indicadoreseconomicos/WebServices/wsIndicadoresEconomicos.asmx/ObtenerIndicadoresEconomicosXML?tcIndicador=317&tcFechaInicio="
urlCompra=$urlCompra$DATE
urlCompra=$urlCompra"&tcFechaFinal="
urlCompra=$urlCompra$DATE
urlCompra=$urlCompra"&tcNombre=dmm&tnSubNiveles=N"

#COMPONER URL VENTA
urlVenta="http://indicadoreseconomicos.bccr.fi.cr/indicadoreseconomicos/WebServices/wsIndicadoresEconomicos.asmx/ObtenerIndicadoresEconomicosXML?tcIndicador=318&tcFechaInicio="
urlVenta=$urlVenta$DATE
urlVenta=$urlVenta"&tcFechaFinal="
urlVenta=$urlVenta$DATE
urlVenta=$urlVenta"&tcNombre=dmm&tnSubNiveles=N"

#LLAMADA PARA OBTENER EL TIPO DE CAMBIO DE COMPRA
curl "$urlCompra" > tipoCambioResult.xml
#OBTENER EL MONTO DE LA SALIDA DEL WEB SERVICE
compra=$(sed -n "/NUM_VALOR/{s/.*&lt;NUM_VALOR&gt;//;s/&lt;\/NUM_VALOR.*//;p;}" tipoCambioResult.xml)

#echo "$compra"

#LLAMADA PARA OBTENER EL TIPO DE CAMBIO DE VENTA
curl "$urlVenta" > tipoCambioResult.xml
#OBTENER EL MONTO DE LA SALIDA DEL WEB SERVICE
venta=$(sed -n "/NUM_VALOR/{s/.*&lt;NUM_VALOR&gt;//;s/&lt;\/NUM_VALOR.*//;p;}" tipoCambioResult.xml)

#echo "$venta"

if [ ! -z "$compra" ] && [ ! -z "$venta" ]; then
	#LLAMADA A PROCEDIMIENTO EN LA BASE DE DATOS PARA REALIZAR LA INSERCION
	DATESP=`date +%d_%m_%Y`
	#echo $DATESP
	#EL PROCEDIMIENTO DEBE LEER LA FECHA CON EL FORMATO QUE SE ENVIA, QUE ES DD_MM_YYYY
	mysql -uUSER -pPASSWORD DATABASE -e "call insertarTipoCambio('$DATESP', $compra, $venta, 'P')"

	#ENVIO DE EMAIL 
	mutt -s "[SAPROIN] Carga tipo de cambio (C:"$compra" V:"$venta")" -- EMAILDESTINO < /FICHEROCUERPOEMAIL
else
	#ENVIO DE EMAIL 
	mutt -s "[SAPROIN] Carga tipo de cambio (ERROR)" -- EMAILDESTINO < /FICHEROCUERPOEMAIL
fi

